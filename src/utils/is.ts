export const is = {
  number: (x: any) => Number.isFinite(+x),
  positiveInt: (x: any) => is.number(x) && x % 1 === 0 && x > 0,
  string: (x: any) => String(x) === x,
};
