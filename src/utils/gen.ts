export const gen = {
  coin: (probability = 0.5) => Math.random() < probability,
  int: (min = 0, max = 1) => Math.floor(Math.random() * (max - min + 1)) + min,
  pick: <Item = string>(list: Item[]): Item => list[gen.int(0, list.length - 1)],
  pickChar: (sample: string): string => sample[gen.int(0, sample.length - 1)],
  scramble: <Item = string>(list: Item[]) => [...list].sort(() => gen.int(-1, 1)),
};

declare global {
  interface Window {
    gen: any;
  }
}
