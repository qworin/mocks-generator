import {PoolMeta} from '../types';

const type = {
  ivan: {m: [0], f: [1]}, // -ович
  ali: {m: [2], f: [3]}, // -евич
  dima: {m: [4], f: [5]}, // Дмитри|евич
  eugen: {m: [6], f: [7]}, // Евген||ьевич
  luka: {m: [8], f: [9]}, // Лук|ич
  mixa: {m: [10], f: [11]}, // Миха||йлович
  lev: {m: [12], f: [13]}, // Л||ьвович
  pasha: {m: [14], f: [15]}, // Пав||лович
  jakob: {m: [16], f: [17]}, // Яковлевич
};

export const names: PoolMeta = {
  endings: [
    'ович',     // 0
    'овна',     // 1
    'евич',     // 2
    'евна',     // 3
    'Sевич',    // 4
    'Sевна',    // 5
    'SSьевич',  // 6
    'SSьевна',  // 7
    'Sич',      // 8
    'Sинична',  // 9
    'SSйлович', // 10
    'SSйловна', // 11
    'SSьвович', // 12
    'SSьвовна', // 13
    'SSлович',  // 14
    'SSловна',  // 15
    'левич',    // 16
    'левна',    // 17
  ],
  list: {
    Александр: type.ivan,
    Алексей: type.dima,
    Али: type.ali,
    Андрей: type.dima,
    Артём: type.ivan,
    Артур: type.ivan,
    Бенедикт: type.ivan,
    Богдан: type.ivan,
    Борис: type.ivan,
    Булат: type.ivan,
    Валерий: type.eugen,
    Василий: type.eugen,
    Виктор: type.ivan,
    Владимир: type.ivan,
    Владислав: type.ivan,
    Гавриил: type.ivan,
    Гарри: type.ali,
    Генрих: type.ivan,
    Георгий: type.dima,
    Глеб: type.ivan,
    Данил: type.ivan,
    Данияр: type.ivan,
    Демид: type.ivan,
    Денис: type.ivan,
    Дмитрий: type.dima,
    Дорофей: type.dima,
    Евгений: type.eugen,
    Егор: type.ivan,
    Елисей: type.dima,
    Ефим: type.ivan,
    Жорж: type.ali,
    Захар: type.ivan,
    Зиновий: type.eugen,
    Иван: type.ivan,
    Игорь: type.dima,
    Илья: type.luka,
    Иннокентий: type.eugen,
    Ипполит: type.ivan,
    Казимир: type.ivan,
    Карл: type.ivan,
    Кирилл: type.ivan,
    Клавдий: type.dima,
    Константин: type.ivan,
    Кузьма: type.luka,
    Лев: type.lev,
    Леонид: type.ivan,
    Лука: type.luka,
    Людвиг: type.ivan,
    Максим: type.ivan,
    Марк: type.ivan,
    Матвей: type.dima,
    Михаил: type.mixa,
    Назар: type.ivan,
    Наум: type.ivan,
    Никита: type.luka,
    Никифор: type.ivan,
    Нургали: type.ali,
    Олег: type.ivan,
    Оскар: type.ivan,
    Павел: type.pasha,
    Петр: type.ivan,
    Платон: type.ivan,
    Потап: type.ivan,
    Роберт: type.ivan,
    Родион: type.ivan,
    Роман: type.ivan,
    Ростислав: type.ivan,
    Руслан: type.ivan,
    Святослав: type.ivan,
    Семён: type.ivan,
    Станислав: type.ivan,
    Степан: type.ivan,
    Тельман: type.ivan,
    Тигран: type.ivan,
    Тимофей: type.dima,
    Тимур: type.ivan,
    Трофим: type.ivan,
    Устин: type.ivan,
    Фёдор: type.ivan,
    Феликс: type.ivan,
    Филипп: type.ivan,
    Фома: type.luka,
    Фридрих: type.ivan,
    Чингиз: type.ivan,
    Шамиль: type.ali,
    Эдуард: type.ivan,
    Эмиль: type.ali,
    Эмир: type.ivan,
    Эммануил: type.ivan,
    Эрнест: type.ivan,
    Юлиан: type.ivan,
    Юрий: type.eugen,
    Яков: type.jakob,
    Ян: type.ivan,
    Ярослав: type.ivan,
  },
};
