import {Response, Request} from 'express';
import {IPersonNameGenerator} from './types';

let generator: IPersonNameGenerator;

export const generate = async (req: Request, res: Response) => {
  if (!generator) {
    const {PersonNameGenerator} = await require('./generator');
    generator = new PersonNameGenerator();
  }
  const {count = 10, details = false, ratio = -1} = req.query;
  const result = generator.genNames({
    count: +count,
    fullInfo: Boolean(details),
    mfRatio: +ratio,
  });
  res.json(result);
};
