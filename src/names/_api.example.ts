import {Response, Request, NextFunction} from 'express';

const users = [
  { name: 'TJ', email: 'tj@vision-media.ca' },
  { name: 'Tobi', email: 'tobi@vision-media.ca' }
];

export const list = (_, res: Response) => {
  res.json(users);
};

export const load = (req: Request, _, next: NextFunction) => {
  const {id} = req.params;
  req['user'] = users[id];
  if (req['user']) {
    next();
  } else {
    const err = new Error('cannot find user ' + id);
    next(err);
  }
};

export const view = (req: Request, res: Response) => {
  res.json({
    title: 'Viewing user ' + req['user'].name,
    user: req['user']
  });
};

export const edit = (req: Request, res: Response) => {
  res.json({
    title: 'Editing user ' + req['user'].name,
    user: req['user']
  });
};

export const update = (req: Request, res: Response) => {
  // Normally you would handle all kinds of
  // validation and save back to the db
  const user = req.body;
  req['user'].name = user.name;
  req['user'].email = user.email;
  res.redirect('back');
};
