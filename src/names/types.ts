export type Gender = 'm' | 'f';

export type GenderEndings = Record<Gender, number[]>;

export interface Pool {
  [prefix: string]: GenderEndings;
}

export type PoolEntry = [string, GenderEndings];

export interface PoolMeta {
  endings: string[];
  list: Pool;
}

export interface GetNameParams {
  count?: number;
  fullInfo?: boolean;
  genders?: Gender[],
  mfRatio?: number;
}

interface FullInfo {
  familyName: string;
  name: string;
  patronymicName: string;
  fullName: string;
  gender: Gender;
}

export interface IPersonNameGenerator {
  genNames(params: GetNameParams): (string | FullInfo)[];
}
