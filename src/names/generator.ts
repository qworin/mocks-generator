import {gen} from '../utils';
import {GetNameParams, IPersonNameGenerator} from './types';
import {familyGenerator, nameGenerator, patronymicGenerator} from './gen';

const DEFAULT_MF_RATIO = 0.5;

export class PersonNameGenerator implements IPersonNameGenerator {
  private genGender = (mfRatio = 0.5) => gen.coin(mfRatio) ? 'm' : 'f';
  private genFamily = familyGenerator();
  private genName = nameGenerator();
  private genPatronymic = patronymicGenerator();

  private genFNPs = (params: GetNameParams) => {
    const {genders, fullInfo = false} = params;
    return genders.map((gender) => {
      const f = this.genFamily(gender);
      const n = this.genName(gender);
      const p = this.genPatronymic(gender);
      const fullName = [f, n, p].join(' ');
      return fullInfo
        ? {familyName: f, name: n, patronymicName: p, fullName, gender}
        : fullName;
    });
  };

  public genNames = (params: GetNameParams) => {
    let {mfRatio = DEFAULT_MF_RATIO, ...otherParams} = params;
    if (mfRatio && (mfRatio < 0 || mfRatio > 1)) mfRatio = DEFAULT_MF_RATIO;
    const count = params.count > 0 ? Math.ceil(params.count) : 1;
    const genders = [...Array(count)].map(() => this.genGender(mfRatio));
    return this.genFNPs({...otherParams, genders});
  };
}
