import {gen} from '../../utils';
import {names} from '../pools';
import {Gender} from '../types';

const {endings, list} = names;
const entries = Object.entries(list);

const build = (prefix: string, ending: string) => {
  ending = ending.replace(/^S+/, (deletions) => {
    prefix = prefix.slice(0, -deletions.length);
    return '';
  });
  return `${prefix}${ending}`;
};

const generator = () => {
  const pool = gen.scramble(entries);
  return (gender?: Gender) => {
    if (!pool.length) pool.push(...gen.scramble(entries));
    const [prefix, genderEndings] = pool.pop();
    const [index] = genderEndings[gender];
    const ending = endings[index];
    return build(prefix, ending);
  };
};

export {generator as patronymicGenerator};
