import {gen} from '../../utils';
import {familyNames} from '../pools';
import {Gender} from '../types';

const DOUBLE_FAMILY_NAME_RATE = 0.02;
const {endings, list} = familyNames;
const entries = Object.entries(list);

const generator = (doubleRate = DOUBLE_FAMILY_NAME_RATE) => {
  const pool = gen.scramble(entries);
  const generate = (gender?: Gender, except?: string) => {
    if (!pool.length) pool.push(...gen.scramble(entries));
    const [prefix, genderEndings] = pool.pop();
    if (except === prefix) return generate(gender, prefix);
    const index = gen.pick(genderEndings[gender]);
    const ending = endings[index];
    return {prefix, ending};
  };
  return (gender: Gender) => {
    const {prefix, ending} = generate(gender);
    let name = `${prefix}${ending}`;
    if (gen.coin(doubleRate)) {
      const {prefix: prefix2, ending: ending2} = generate(gender, prefix);
      name += `-${prefix2}${ending2}`;
    }
    return name;
  };
};

export {generator as familyGenerator};
