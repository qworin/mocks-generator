import {gen} from '../../utils';
import {names, femaleNames} from '../pools';
import {Gender} from '../types';

const maleNames = Object.keys(names.list)

const generator = () => {
  const female = gen.scramble(femaleNames);
  const male = gen.scramble(maleNames);
  return (gender: Gender) => {
    if (!female.length) female.push(...gen.scramble(femaleNames));
    if (!male.length) male.push(...gen.scramble(maleNames));
    if (gender === 'f') return female.pop();
    return male.pop();
  };
};

export {generator as nameGenerator};
