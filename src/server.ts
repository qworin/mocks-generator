import * as names from './names';
import * as fish from './markov';
import express, {Response} from 'express';

const app: express.Application = express();
const port = 3010;

const log = (req, _, next) => {
  console.log(req.query);
  next();
}

app.use(log);
app.use(express.json());
app.get('/', (_, res: Response) => res.send('Hello World!'));

//----------- names
/**
 * Generate list of russian names.
 * 
 * @param count: default 10; list size
 * @param ratio: default 0.5; male/female names distribution ratio
 * @param details: default false; provide names as objects
 *
 * @returns string[]
 * or
 * @returns {fullName: string; gender: 'f'|'m'; familyName: string; name: string; patronymicName: string}[]
 */
app.get('/names', names.generate);

//----------- fish texts
/**
 * Generate a fish text. Request examples:
 * /fish
 * /fish?type=habr&seed=патенты
 * /fish?paragraphs=3&sample=8
 *
 * @param type: legal|habr, default legal; corpus of text used for generation
 * @param sample: default 2; bigger value = better quality, but slower
 * @param paragraphs: default 5
 *
 * @returns string
 */
app.get('/fish', fish.generate);

app.listen(port, () => {
  console.log(`Mocks app listening at http://localhost:${port}`)
});
