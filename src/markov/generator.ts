import {gen} from '../utils';
import {Tokenizer} from './tokenizer';

const DEFAULT_PARAGRAPHS_COUNT = 5;

interface TransitionsMap {
  [from: string]: string[]
}

export class Predict {
  private transitionsMap!: TransitionsMap;
  private static readonly DEFAULT_SAMPLE_SIZE = 2;
  private sampleSize = Predict.DEFAULT_SAMPLE_SIZE;
  private corpusTokens!: string[];


  constructor(corpus: string, sampleSize?: number) {
    const start = performance.now();
    this.corpusTokens = Tokenizer.tokenize(corpus);
    this.init(sampleSize);
    console.log(`Init @${performance.now() - start}`)
  }
  
  private init = (sampleSize?: number) => {
    if (sampleSize !== undefined && sampleSize >= Predict.DEFAULT_SAMPLE_SIZE) {
      this.sampleSize = Math.floor(sampleSize);
    }
    const samples = Tokenizer.slice(this.corpusTokens, this.sampleSize);
    this.transitionsMap = Predict.buildTransitions(samples);
  };

  public setSampleSize = (size?: number) => {
    if (size === this.sampleSize) return;
    this.init(size);
  };

  public run = ({
    seed = '',
    words,
    paragraphs,
  }: {seed?: string; words?: number; paragraphs?: number} = {}) => {
    const start = performance.now();
    const generator = this.generateChain(seed);
    const genWord = () => (generator.next().value as string);
    if (words) {
      const result = Tokenizer.sanitize([...Array(words)].map(genWord));
      console.info(`Generated ${words} words in ${performance.now() - start}`);
      return result;
    }
    const _paragraphs = paragraphs || DEFAULT_PARAGRAPHS_COUNT;
    const tokens: string[] = [];
    let count = 0;
    while (count < _paragraphs) {
      const word = genWord();
      tokens.push(word);
      if (Tokenizer.hasNewLine(word)) count++;
      if (count === _paragraphs) tokens.pop();
      if (tokens.length > 1e4) break;
    }
    const result = Tokenizer.sanitize(tokens);
    console.info(`Generated ${tokens.length} words, ${_paragraphs} paragraphs in ${performance.now() - start}`);
    return result;
  };

  private predict = (chain: string[]) => {
    const last = chain.slice(-(this.sampleSize - 1));
    let nextWords = this.transitionsMap[Tokenizer.keyFrom(last)];
    if (!nextWords) nextWords = this.createChain(last[0]);
    return gen.pick(nextWords);
  };

  private *generateChain(seed?: string) {
    let tries = 0;
    let chain = this.createChain(seed);
    if (!this.predict(chain)) chain = this.createChain();
    for (const chunk of chain) { yield chunk }
    do {
      const state = this.predict(chain);
      yield state as string;
      console.assert(!!state, state, [...chain])
      state ? chain.push(state) : tries++;
    } while (tries < 1);
    throw new Error('Unexpected end of generator');
  }

  private createChain = (seed?: string): string[] =>
    Tokenizer.unkey(this.pickTransition(seed));

  private get transitionKeys() {
    return Object.keys(this.transitionsMap);
  }

  private pickTransition = (match?: string): string => {
    let key = match
      ? gen.pick(this.transitionKeys.filter((key) => key.includes(match)))
      : gen.pick(this.transitionKeys);
    if (!key) key = gen.pick(this.transitionKeys);
    return key;
  }

  private static buildTransitions = (samples: string[][]) => samples
    .reduce((transitions: TransitionsMap, sample) => {
      const restTokens = [...sample]; 
      const next = restTokens.pop() as string;
      const state = Tokenizer.keyFrom(restTokens);
      if (!transitions[state]) transitions[state] = [];
      transitions[state].push(next);
      return transitions;
    }, {});
}
