import {Response, Request} from 'express';
import {Predict} from './generator';
import {is} from '../utils';

enum TYPE {
  LEGAL = 'legal', // default
  HABR = 'habr',
}
const corpusPath = {
  [TYPE.LEGAL]: '../assets/legal.json',
  [TYPE.HABR]: '../assets/habr.json',
}
// empty due to lazy-load
const generator: Record<TYPE, Predict> = {
  [TYPE.LEGAL]: null,
  [TYPE.HABR]: null,
}

const loadGenerator = async (type: TYPE) => {
  if (generator[type]) return;
  const corpus = await require(corpusPath[type]);
  generator[type] = new Predict(corpus);
};

const setDef = (raw: any, defValue: number | string = 0) => {
  if (typeof defValue === 'string') return is.string(raw) ? raw : defValue;
  return is.positiveInt(raw) ? +raw : defValue;
}

export const generate = async (req: Request, res: Response) => {
  const {type: rawType, seed = '', paragraphs = 0, sample} = req.query;
  const type = rawType === TYPE.HABR ? TYPE.HABR : TYPE.LEGAL;
  await loadGenerator(type);
  if (is.number(sample)) generator[type].setSampleSize(+sample);
  const result = generator[type].run({
    seed: setDef(seed, ''),
    paragraphs: setDef(paragraphs),
  });
  return res.json(result);
};
