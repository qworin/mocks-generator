const NEWLINE_PLACEHOLDER = '§';
const PARAGRAPH_CHARACTER = '\n';
const KEY_PREFIX = '$/$/$';

const newlinesRE = /\n\s*/g;

const punctuation = `[](){}!?.,:;'"\/*&^%$_+-–—=<>@|~`.split('').join('\\');
const ellipsis = '\\.{3}';
const words = '[a-zA-Zа-яА-ЯёЁ]+';
const compounds = `${words}-${words}`;
const stripBeginningRE = new RegExp(`^[${punctuation}]+\s*`, 'g');
const tokenizeRE = new RegExp(`(${ellipsis}|${compounds}|${words}|[${punctuation}])`);
const newlineRE = new RegExp(`${NEWLINE_PLACEHOLDER}`);

export class Tokenizer {
  static slice = (tokens: string[], sampleSize: number): string[][] =>
    tokens
      .map((_, index, all) => all.slice(index, index + sampleSize))
      .filter((group) => group.length === sampleSize);

  static tokenize = (text: string) =>
    text
      .replace(newlinesRE, NEWLINE_PLACEHOLDER)
      .replace(stripBeginningRE, '')
      .split(tokenizeRE)
      .filter(Boolean);

  static keyFrom = (tokens: string[]) => `${KEY_PREFIX}${tokens.join('')}`;

  static unkey = (key: string): string[] =>
    key
      .replaceAll(KEY_PREFIX, '')
      .split(tokenizeRE)
      .filter(Boolean);

  static sanitize = (tokens: string[]) =>
    tokens
      .join('')
      .replaceAll(NEWLINE_PLACEHOLDER, PARAGRAPH_CHARACTER)
      .replaceAll(KEY_PREFIX, '')
      .trim();

  static hasNewLine = (sample: string) => newlineRE.test(sample);
}
